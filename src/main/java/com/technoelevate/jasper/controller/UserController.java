package com.technoelevate.jasper.controller;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.technoelevate.jasper.entity.User;
import com.technoelevate.jasper.service.UserService;

import net.sf.jasperreports.engine.JRException;

@RestController
public class UserController {
	
	@Autowired
	private UserService service;
	@PostMapping("/add")
	public User saveUser(@RequestBody User user) {
		return service.saveUser(user);
	}
	
	@GetMapping("/get")
	public List<User> getUsers(){
		return service.getUsers();
	}
	
	@GetMapping("/export/{format}")
	private String exportReport(@PathVariable String format) throws FileNotFoundException, JRException, SQLException {
		return service.exportReport(format);
	}

}
