package com.technoelevate.jasper.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import com.technoelevate.jasper.entity.User;
import com.technoelevate.jasper.repository.UserRepository;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Service
public class UserService {
    @Autowired
	private UserRepository repo;
    
    @Autowired
    DataSource dataSource;
    
    public User saveUser(User user) {
     return  repo.save(user);		
	}
	
	public List<User> getUsers(){
		return repo.findAll();
		
	}
	
	public String exportReport(String format) throws JRException, FileNotFoundException, SQLException {
	  // List<User> users = repo.findAll();
		String path="F:\\jasper";
		//Load file and compile it
		File file=ResourceUtils.getFile("classpath:users.jrxml");
		JasperReport jasper=JasperCompileManager.compileReport(file.getAbsolutePath());
		//to get the users
//		JRBeanCollectionDataSource datasource=new JRBeanCollectionDataSource(users);
		Map<String,Object> parameters=new HashMap<>();
		parameters.put("createdBY", "fayaz");
		JasperPrint jasperPrint=JasperFillManager.fillReport(jasper,parameters,dataSource.getConnection());
		if(format.equalsIgnoreCase("html")) {
			JasperExportManager.exportReportToHtmlFile(jasperPrint,path+"\\users.html");
		}
		if(format.equalsIgnoreCase("pdf")) {
			JasperExportManager.exportReportToPdfFile(jasperPrint,path+"\\users.pdf");
		}
		return "path :"+path;
	}
	
}
