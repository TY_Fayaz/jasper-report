package com.technoelevate.jasper.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.technoelevate.jasper.entity.User;

public interface UserRepository extends JpaRepository<User, Integer>{

}
